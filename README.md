# Repository status

Master repo: https://gitlab.com/fccagou/mydashboard


# Quick run (but Unsafe way)

Run the microservice

    python3 ./mydashboard -d -v --notoken --conf data/conf.json


To make a user systemd servicea (still issue because of environment vars)

    mkdir -p ~/.config/systemd/user
    curl --silent http://localhost:8080/systemd/service -o ~/.config/systemd/user/mydashboard.service
    systemctl --user enable mydashboard
    systemctl --user start mydashboard



# Safe way

In multi users environment, commands can be run by any connected user using
above commands. To avoid this security issue, remove the --notoken parameter.

Mydashboard will generate a token that must be pass as a cookie in every http
requests. This token is written in ~/.mydashboard file.

So, the token way to run mydashbard is :

    python3 ./mydashboard -d -v --conf data/conf.json

    curl  -b "MYTOKEN=$(cat ~/.mydashboard)" http://localhost:8080/doc
    curl  -b "MYTOKEN=$(cat ~/.mydashboard)" http://localhost:8080/ui


# Developement mode

/!\ You can only run this mode with the git repository. /!\

## Launch frontend

Move in __docroot__ and install and run development mode.
``` bash
cd docroot
npm install
npm run dev
```

## Launch backend

Run the python program in unsafe way with the following command:
``` bash
python3 ./mydashboard -d -v --notoken --devmode --conf data/conf.json
```

# The web GUI
## At startup

![The web GUI](doc/mydashboard_run.png) "The web browser launched to access UI"

## Catch remote error
![remote ko](doc/mydashboard_remote_ko.png) "Remote error"

## Backend stopped
![remote ko](doc/mydashboard_backend_ko.png) "Backend stopped"


# More documentation

```bash
$ python3  mydashboard --help
usage: mydashboard [-h] [--conf CONF] [--port PORT] [--verbose] [--debug] [--debugfilename DEBUGFILENAME] [--daemon] [--pid] [--pidfile PIDFILE] [--documentroot DOCUMENTROOT] [--remotelist REMOTELIST]
                   [--menuconf MENUCONF] [--remotecnx REMOTECNX] [--notoken] [--nogui] [--webbrowser WEBBROWSER] [--devmode] [--userprefs USERPREFS]

Notify processor.

options:
  -h, --help            show this help message and exit
  --conf CONF, -c CONF  Configuration file.
  --port PORT, -p PORT  Listening port for status push .
  --verbose, -v         verbose.
  --debug, -d           Debug mode.
  --debugfilename DEBUGFILENAME
                        Debug file name mode.
  --daemon              Enable daemon mode.
  --pid                 Write pid file (see --pidfile)
  --pidfile PIDFILE     Set the pid file.
  --documentroot DOCUMENTROOT, -D DOCUMENTROOT
                        Document root for static web pages.
  --remotelist REMOTELIST, -R REMOTELIST
                        Url to json remote list
  --menuconf MENUCONF, -M MENUCONF
                        Url to json menu configuration
  --remotecnx REMOTECNX, -C REMOTECNX
                        Remote connection script
  --notoken             Unsafe mode without token.
  --nogui               Launch grahical user interface
  --webbrowser WEBBROWSER
                        Select web browser (default: /usr/bin/firefox)
  --devmode             Use JS developpement dependencies (can only be used with the git repository)
  --userprefs USERPREFS
                        File used to store user preferences

```

# How it works

## General description

_All configuration files below are sample used to run the tests. They can be changed
using different paths using [program parameters](#more-documentation)_

_You can also use puppet module [puppet-mydashboard](https://gitlab.com/fccagou/puppet-mydashboard)_


`mydashboard` runs a `web client` and serves pages. It uses [conf
file](data/conf.json) to know how to serve data.

- **gui**: "True" / "False" to run/not run the web client
- **remotelist**: path to file containing [remotes list and
    configurations](data/remote.json)
- **webbrowser**: path to web client to run
- **documentroot**: path to directory containing [web ui](docroot)
- **remote_connection**: path to program used to launch [remote
    connections](data/remote_connection.sh)
- **debug_filename**: path to file for debug logs when  `-d` parametr is passed.

## remote connections

`mydashboard` uses environment variables to pass variables to remote connection programs.

All the variables set by `mydashboard` are prefixed by `API_`. The common one
are

- **API_REMOTE_HOST**: the remote host we need to access
- **API_HOST_ALIAS**: data used for documentation/log purpose
- **API_SITE**: the site/DC where the host is located
- **API_DOMAIN**: used for rdp connection
- **API_GROUP**: used to specified the group the host belong to
- **API_PROTOCOL**: protocol used to connect the host. Actually **ssh** and
    **rdp** are supported.
- **API_PROTOCOL_XXX**: specifics data depending on protocol, for example,
  - API_PROTOCOL_SEC={nla,...} for `rdp`
  - API_PROTOCOL_PORT={22,3389,...} for both `rdp` and `ssh`

**ssh connection**:
- is run using `xterm`
- more details in [data/remote_connection.sh](data/remote_connection.sh)

**rdp connection**:
- is run using `xfreerdp`
- more details in [data/remote_connection.sh](data/remote_connection.sh)

## User interface

- **menu on top** is built using [data/menu.json](data/menu.json).
- **hosts displayed** using [data/remote.json](data/remote.json)

## SSO using kerberos

For security purpose, it's interesting to download remote data using kerberos sso.

Actually, mydashboard implements a simple way to allow using current user kerberos
context when exists.

At startup, mydashboard selects the method to use
- [python-requests-gssapi](https://github.com/pythongssapi/requests-gssapi) if installed
- `/usr/bin/curl` _(--negotiate -u: ...)_ if installed

Fallback to `urllib.request.urlopen` method without kerberos support.


# Status

`mydashboard` **version 0.1.1** is actually used in production to access windows systems using
RDP protocol.

A new phase of development is beginning to:

- [ ] add more user customization _(display resolution, remote username, full
    or multiscreen, display color ...)_
- [ ] add host customization _(protocol parameters, display icon, ...)_
- [ ] add dynamics remote filter using user filters or rights
- [ ] add UI display customization.
