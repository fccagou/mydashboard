import { createWebHistory, createRouter } from 'vue-router'
import RemoteAccessView from '../views/RemoteAccess.vue'
import NotFoundView from '../views/NotFound.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'RemoteAccess',
      component: RemoteAccessView,
    },
    {
      path: '/user/preferences',
      name: 'UserPreferences',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/UserPreferences.vue'),
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'NotFound',
      component: NotFoundView,
    },
  ],
})

export default router
