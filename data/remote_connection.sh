#!/usr/bin/bash
#
# For enhancement
# set -o errexit
# set -o nounset
# set -o pipefail

SITE="${API_SITE}"
GROUP="${API_GROUP}"
DOMAIN="${API_DOMAIN}"
PROTOCOL="${API_PROTOCOL}"
REMOTE_HOST="${API_REMOTE_HOST}"
REMOTE_USER="${API_REMOTE_USER:-$USER}"

show_env() {
	printenv | grep ^API_ | sort | awk '{ print "    "$0}'
}


check_connection_or_exit() {
	local host
	local port

	host="$1"
	port="$2"

    if ! timeout --preserve-status 3 bash -c "</dev/tcp/$host/$port" >/dev/null 2>&1
	then
		echo "Host $host unreachable on port $port" >/dev/stderr
		show_env >/dev/stderr
		exit 1
	fi
}


ssh_connection() {

	local remote_port="${API_PROTOCOL_PORT:-22}"
	check_connection_or_exit "${REMOTE_HOST}" "${remote_port}"

	statusfile="$(/usr/bin/mktemp -p /dev/shm)"
	errfile="$(/usr/bin/mktemp -p /dev/shm)"
	xterm_errfile="$(/usr/bin/mktemp -p /dev/shm)"

	DISPLAY=:0
	/usr/bin/xterm \
		-display "$DISPLAY" \
		-T "Connecting to ${REMOTE_USER}@${SITE}/${DOMAIN}/${GROUP}/${REMOTE_HOST}:${remote_port}"\
		-e "ssh -p ${remote_port} ${REMOTE_USER}${REMOTE_HOST} 2>\"${errfile}\"; echo \$? > \"${statusfile}\"" 2> "${xterm_errfile}"

	ret=$?
	if [ "${ret}" == "0" ]
	then
	    ret="$(cat "${statusfile}")"
		cat "${errfile}" >/dev/stderr
	else
		cat "${xterm_errfile}" > /dev/stderr
	fi
	env | sort > /dev/stderr
	/bin/rm "${statusfile}" "${errfile}" "${xterm_errfile}"
	exit ${ret}
}

rdp_connection () {
	local remote_port="${API_PROTOCOL_PORT:-3389}"
	check_connection_or_exit "${REMOTE_HOST}" "${remote_port}"

	"$(dirname "$0")"/rdp_connection.sh \
		--site "$SITE" \
		--group "$GROUP" \
		--sec   "${API_PROTOCOL_SEC}" \
		"${REMOTE_USER}" \
		"${DOMAIN}" \
		"${HOSTNAME}" 2> "$errfile"
	show_env >/dev/stderr
	exit $?
}



if [ "${PROTOCOL}" == "ssh" ] || [ "${GROUP}" == "bleu" ]
then
	ssh_connection
elif [ "$PROTOCOL" == "rdp" ]
then
	rdp_connection
else
	echo "Error, protocol [$PROTOCOL] not implemented"
	exit 1
fi

