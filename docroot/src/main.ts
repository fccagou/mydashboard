import { createApp, type DirectiveBinding } from 'vue'
import { createPinia } from 'pinia'
import router from './router'
import App from './App.vue'

import './assets/scss/styles.scss'

import axios from 'axios'
axios.defaults.baseURL = import.meta.env.VITE_APP_API_ENDPOINT

const app = createApp(App)

app.use(createPinia())
app.use(router)

declare global {
  interface HTMLElement {
    clickOutsideEvent?: (event: MouseEvent) => void
  }
}

const clickOutside = {
  beforeMount: (el: HTMLElement, binding: DirectiveBinding) => {
    el.clickOutsideEvent = (event: MouseEvent) => {
      // here I check that click was outside the el and his children
      if (!(el == event.target || el.contains(event.target as Node))) {
        // and if it did, call method provided in attribute value
        binding.value()
      }
    }
    document.addEventListener('mousedown', el.clickOutsideEvent)
  },
  unmounted: (el: HTMLElement) => {
    if (el.clickOutsideEvent) {
      document.removeEventListener('mousedown', el.clickOutsideEvent)
    }
  },
}
app.directive('click-outside', clickOutside)

app.mount('#app')
