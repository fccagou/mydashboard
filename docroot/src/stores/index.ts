import type { Toast } from '@/models'
import { defineStore } from 'pinia'

const TOASTS_MAX = 10

export const useToastsStore = defineStore('toasts', {
  state: () => {
    return {
      toasts: [] as Toast[],
    }
  },
  actions: {
    addToast(toast: Toast) {
      if (this.toasts.length >= TOASTS_MAX) {
        return
      }
      toast.uuid = generateUUIDv4()
      this.toasts.push(toast)
    },
    removeToast(uuid: string) {
      this.toasts = this.toasts.filter((toast) => toast.uuid != uuid)
    },
  },
})

function generateUUIDv4(): string {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (char) => {
    const random = (Math.random() * 16) | 0
    const value = char === 'x' ? random : (random & 0x3) | 0x8
    return value.toString(16)
  })
}
