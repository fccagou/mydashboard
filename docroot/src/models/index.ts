import type { Ref } from 'vue'

export enum ToastType {
  Success = 'success',
  Warning = 'warning',
  Danger = 'danger',
}

export interface Toast {
  uuid?: string
  type: ToastType
  title: string
  body?: string
  delaySecond: number
  modal?: {
    title: string
    body: string
  }
}

export interface ConfigHost {
  proto: string
  site: string
  domain: string
  group: string
  'proto-config'?: { [key: string]: string }
}

export interface Host {
  uuid: string
  name: string
  alias: string | undefined
  config: ConfigHost
}

export interface Site {
  name: string
  domains: Array<Domain>
}

export interface Domain {
  name: string
  groups: Array<Group>
}

export interface Group {
  name: string
  hosts: Array<Host>
}

export interface OrderingDomain {
  name: string
  order: number
  groups?: OrderingGroup[]
}

export interface OrderingGroup {
  name: string
  order: number
}

export interface OrderingSite {
  name: string
  order: number
  domains?: OrderingDomain[]
}

export type Ordering = OrderingSite[]

export interface Connection {
  hostUUID: string
  hostname: string
  domain?: string
  site?: string
  loading_ref: Ref<boolean>
  event?: MouseEvent
}

export enum PreferenceType {
  Boolean = 'bool',
  Number = 'number',
  Resolution = 'resolution',
}

export interface Preference {
  name: string
  type: PreferenceType
  value: PreferenceValue
  default: PreferenceValue
}

export type PreferenceValue = string | Resolution | boolean

export interface Resolution {
  width: number | undefined
  height: number | undefined
}

interface Api<T> {
  [key: string]: T
}

export type ApiResponse<T> = Api<T> & {
  errors: Array<string>
}
