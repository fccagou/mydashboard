#! /usr/bin/env python
# -*- encoding: utf-8 -*-

# This file is part of mydashboard.
# Copyright 2019-2021 fccagou <me@fccagou.fr>
#
# mydashboard is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mydashboard is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mydashboard. If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

setup(
    name="mydashboard",
    version="0.1.2",
    description="Userland Dashboard microservice",
    url="http://github.com/fccagou/mydashboard",
    author="fccagou",
    author_email="me@fccagou.fr",
    license="GPLv3+",
    long_description="Userland Dashboard",
    scripts=["mydashboard"],
    packages=[],
    data_files=[
        (
            "share/doc/mydashboard",
            [
                "doc/README.md",
                "doc/LICENSE.md",
            ],
        ),
        (
            "share/doc/mydashboard/samples",
            [
                "data/conf.json",
                "data/rdp_connection.sh",
                "data/remote_connection_mock.sh",
                "data/remote_connection.sh",
                "data/remote.json",
            ],
        ),
        ("/etc/mydashboard", ["data/mydashboard.conf-default"]),
        (
            "/etc/mydashboard/html",
            [
                "docroot/index.html",
            ],
        ),
        (
            "/etc/mydashboard/html/assets",
            [
                os.path.join("docroot/dist/assets", x)
                for x in os.listdir("docroot/dist/assets")
            ],
        ),
    ],
)
